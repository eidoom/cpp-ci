# [cpp-ci](https://gitlab.com/eidoom/cpp-ci)

Uses [eidoom/pre-commit-hooks-cpp](https://gitlab.com/eidoom/pre-commit-hooks-cpp).

Update pre-commit hooks with `pre-commit autoupdate`.

`clang-check` does not currently work so is commented out (see hook repo).
To generate the compilation database file `compile_commands.json` that it needs, use [Bear](https://github.com/rizsotto/Bear): `bear -- make`.

`clang-format` works locally but the remote CI call fails - something to do with Docker in Docker.
